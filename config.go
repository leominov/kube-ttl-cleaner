package main

import (
	"time"

	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v3"
)

type Config struct {
	DryRun        bool          `envconfig:"DRY_RUN" yaml:"dry_run"`
	Debug         bool          `envconfig:"DEBUG" yaml:"debug"`
	DefaultTTL    time.Duration `envconfig:"DEFAULT_TTL" yaml:"default_ttl" default:"168h" required:"true"`
	VaultRoleName string        `envconfig:"VAULT_ROLE_NAME" yaml:"vault_role_name" default:"ttl-cleaner" required:"true"`
}

func LoadConfigFromEnv() (*Config, error) {
	c := &Config{}
	return c, envconfig.Process("", c)
}

func (c *Config) String() string {
	b, _ := yaml.Marshal(c)
	return string(b)
}
