package main

import (
	"context"
	"errors"
	"fmt"
	"path"
	"strings"

	vault "github.com/hashicorp/vault/api"
	auth "github.com/hashicorp/vault/api/auth/kubernetes"
)

type VaultInterface interface {
	DeletePath(path string) error
}

type VaultClient struct {
	cli *vault.Client
}

func NewVaultClient(roleName string) (*VaultClient, error) {
	config := vault.DefaultConfig()

	client, err := vault.NewClient(config)
	if err != nil {
		return nil, fmt.Errorf("unable to initialize Vault client: %w", err)
	}

	k8sAuth, err := auth.NewKubernetesAuth(roleName)
	if err != nil {
		return nil, fmt.Errorf("unable to initialize Kubernetes auth method: %w", err)
	}

	authInfo, err := client.Auth().Login(context.TODO(), k8sAuth)
	if err != nil {
		return nil, fmt.Errorf("unable to log in with Kubernetes auth: %w", err)
	}
	if authInfo == nil {
		return nil, fmt.Errorf("no auth info was returned after login")
	}

	v := &VaultClient{
		cli: client,
	}

	return v, nil
}

func (v *VaultClient) DeletePath(route string) error {
	secret, err := v.cli.Logical().List(route)
	if err != nil {
		return err
	}
	_, secrets, err := v.listPath(secret)
	if err != nil {
		return err
	}
	var errs []string
	for _, s := range secrets {
		_, err := v.cli.Logical().Delete(path.Join(route, s))
		if err != nil {
			errs = append(errs, err.Error())
		}
	}
	if len(errs) > 0 {
		return errors.New(strings.Join(errs, ", "))
	}
	return nil
}

func (v *VaultClient) listPath(secret *vault.Secret) ([]string, []string, error) {
	if secret == nil || secret.Data == nil || secret.Data["keys"] == nil {
		return nil, nil, errors.New("secret keys not found")
	}
	keys := secret.Data["keys"].([]interface{})
	var (
		paths   []string
		secrets []string
	)
	for _, k := range keys {
		record := k.(string)
		if record[len(record)-1] == '/' {
			paths = append(paths, k.(string))
		} else {
			secrets = append(secrets, k.(string))
		}
	}
	return paths, secrets, nil
}
