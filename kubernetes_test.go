package main

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
)

func TestRemovePVCCollection(t *testing.T) {
	k8sClient := fake.NewSimpleClientset(
		&v1.PersistentVolumeClaim{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: "ns-a",
				Name:      "pvc-a",
				Labels: map[string]string{
					"app.kubernetes.io/instance": "release-1",
				},
			},
		},
		&v1.PersistentVolumeClaim{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: "ns-a",
				Name:      "pvc-b",
				Labels: map[string]string{
					"app.kubernetes.io/instance": "release-1",
				},
			},
		},
		&v1.PersistentVolumeClaim{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: "ns-b",
				Name:      "pvc-c",
				Labels: map[string]string{
					"app.kubernetes.io/instance": "release-1",
				},
			},
		},
		&v1.PersistentVolumeClaim{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: "ns-a",
				Name:      "pvc-d",
				Labels: map[string]string{
					"app.kubernetes.io/instance": "release-2",
				},
			},
		},
	)

	err := RemovePVCCollection(k8sClient, "ns-a", "release-1")
	assert.NoError(t, err)

	_, err = k8sClient.CoreV1().PersistentVolumeClaims("ns-a").Get(context.Background(), "pvc-a", metav1.GetOptions{})
	assert.Error(t, err)

	_, err = k8sClient.CoreV1().PersistentVolumeClaims("ns-a").Get(context.Background(), "pvc-b", metav1.GetOptions{})
	assert.Error(t, err)

	_, err = k8sClient.CoreV1().PersistentVolumeClaims("ns-b").Get(context.Background(), "pvc-c", metav1.GetOptions{})
	assert.NoError(t, err)

	_, err = k8sClient.CoreV1().PersistentVolumeClaims("ns-a").Get(context.Background(), "pvc-d", metav1.GetOptions{})
	assert.NoError(t, err)
}
