package main

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

var (
	ttlAnnotation              = "pltf_ttl"
	releaseNamespaceAnnotation = "meta.helm.sh/release-namespace"
	releaseNameAnnotation      = "meta.helm.sh/release-name"
)

type Cleaner struct {
	c           *Config
	helm        HelmInterface
	kubeClient  kubernetes.Interface
	log         *logrus.Entry
	vaultClient VaultInterface
}

func (c *Cleaner) Run() (int, error) {
	configMapList, err := c.kubeClient.CoreV1().ConfigMaps("").List(context.Background(), metav1.ListOptions{
		LabelSelector: "monitoring_scope,branch!=master",
	})
	if err != nil {
		return 0, err
	}
	c.log.Debugf("Found ConfigMaps: %d", len(configMapList.Items))
	var deletedEnvs int
	for _, cm := range configMapList.Items {
		if !strings.HasSuffix(cm.Name, "-cm") {
			continue
		}
		if ok, err := c.processConfigMap(cm); err != nil {
			c.log.WithError(err).Errorf("Failed to process ConfigMap: %s", cm.Name)
		} else if ok {
			deletedEnvs++
		}
	}
	return deletedEnvs, nil
}

func (c *Cleaner) processConfigMap(cm v1.ConfigMap) (bool, error) {
	branch := cm.Labels["branch"]
	projectName := cm.Labels["name"]

	log := c.log.WithFields(logrus.Fields{
		"branch":             branch,
		"creation_timestamp": cm.CreationTimestamp,
		"name":               projectName,
		"namespace":          cm.Namespace,
		"release":            cm.Name,
	})

	releaseNamespace, ok := cm.Annotations[releaseNamespaceAnnotation]
	if !ok {
		log.Debug("Skipped, release-namespace not found")
		return false, nil
	}

	releaseName, ok := cm.Annotations[releaseNameAnnotation]
	if !ok {
		log.Debug("Skipped, release-namespace not found")
		return false, nil
	}

	var (
		ttl time.Duration
		err error
	)
	ttlRaw, ok := cm.Annotations[ttlAnnotation]
	if ok {
		ttl, err = ParseTTL(ttlRaw)
		if err != nil {
			return false, err
		}
	} else {
		// В кластере попадаются стенды, у которых нет ttl, потому что он был
		// введен много позже инициализации переезда на helm3, поэтому пытаемся
		// от таких стендов избавиться
		ttl = c.c.DefaultTTL
	}

	if cm.CreationTimestamp.Add(ttl).After(time.Now()) {
		log.Debug("Skipped, ttl not expired")
		return false, nil
	}

	expiredIn := humanize.Time(cm.CreationTimestamp.Add(ttl))
	log.Infof("Release expired %s [ttl=%s", expiredIn, ttl)

	if !c.c.DryRun {
		err := c.helm.Uninstall(releaseNamespace, releaseName)
		if err != nil {
			return false, err
		}

		err = RemovePVCCollection(c.kubeClient, releaseNamespace, releaseName)
		if err != nil {
			log.Warnf("Failed to remove %s/%s PVC(s): %v", releaseNamespace, releaseName, err)
		}

		vaultPath := fmt.Sprintf("configs/metadata/%s/%s", projectName, branch)
		err = c.vaultClient.DeletePath(vaultPath)
		if err != nil {
			log.Warnf("Failed to remove %s vault data: %v", vaultPath, err)
		}
	}

	log.Infof("Release %s/%s stopped", releaseNamespace, releaseName)

	return true, nil
}
