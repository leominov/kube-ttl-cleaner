# kube-ttl-cleaner

Клинер для тестовых стендов. Используется в кластерах pltf-stage и pltf-preprod.

## Ссылки

* https://gitlab.qleanlabs.ru/platform/infrastructure/-/tree/master/manifests/apps/ttl-cleaner
* https://qleanru.atlassian.net/browse/INF-811
