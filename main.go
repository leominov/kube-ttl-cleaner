package main

import (
	"flag"
	"fmt"

	"github.com/sirupsen/logrus"
)

var (
	validate = flag.Bool("validate", false, "Validate configuration end exit.")
)

func main() {
	flag.Parse()

	l := logrus.New()
	l.SetFormatter(&logrus.JSONFormatter{})
	logger := l.WithField("app", "kube-ttl-cleaner")

	config, err := LoadConfigFromEnv()
	if err != nil {
		logger.WithError(err).Fatal("Configuration failed")
	}

	if config.Debug {
		l.SetLevel(logrus.DebugLevel)
	}

	if *validate {
		fmt.Println(config.String())
		return
	}

	logger.Info("Initializing Kubernetes client...")
	kubeClient, err := NewKubernetesClient()
	if err != nil {
		logger.WithError(err).Fatal("Failed to initialize Kubernetes client")
	}

	logger.WithField("role", config.VaultRoleName).Info("Initializing Vault client...")
	vaultClient, err := NewVaultClient(config.VaultRoleName)
	if err != nil {
		logger.WithError(err).Fatal("Failed to initialize Vault client")
	}

	logger.Info("Initializing Helm...")
	helm, err := NewHelm(logger)
	if err != nil {
		logger.WithError(err).Fatal("Failed to initialize Helm")
	}

	cleaner := &Cleaner{
		c:           config,
		helm:        helm,
		kubeClient:  kubeClient,
		log:         logger,
		vaultClient: vaultClient,
	}

	logger.Info("Starting cleanup...")
	_, err = cleaner.Run()
	if err != nil {
		logger.WithError(err).Fatal("Cleanup failed")
	}

	logger.Info("Done")
}
