FROM golang:1.16-alpine3.13 AS builder
WORKDIR /go/src/gitlab.qleanlabs.ru/platform/infra/kube-ttl-cleaner
COPY . .
RUN go build .

FROM alpine:3.13
COPY --from=builder /go/src/gitlab.qleanlabs.ru/platform/infra/kube-ttl-cleaner/kube-ttl-cleaner /usr/bin/
COPY --from=eu.gcr.io/qlean-242314/gitlab/platform/infra/pgwrun /pgwrun /usr/bin/pgwrun
RUN wget -qO - https://get.helm.sh/helm-v3.7.1-linux-amd64.tar.gz | tar -zxOf - linux-amd64/helm > /usr/bin/helm && \
    chmod +x /usr/bin/helm
