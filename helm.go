package main

import (
	"fmt"
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
)

type HelmInterface interface {
	Uninstall(namespace, release string) error
}

type Helm struct {
	helmBin string
	log     *logrus.Entry
}

func NewHelm(log *logrus.Entry) (*Helm, error) {
	helmBin, err := exec.LookPath("helm")
	if err != nil {
		return nil, err
	}
	h := &Helm{
		helmBin: helmBin,
		log:     log,
	}
	return h, nil
}

func (h *Helm) Uninstall(namespace, release string) error {
	helmArgs := []string{"uninstall", fmt.Sprintf("--namespace=%s", namespace), release}
	h.log.Debugf("Running: helm %s", strings.Join(helmArgs, " "))

	cmd := exec.Command(h.helmBin, helmArgs...)
	cmd.Stdout = h.log.WriterLevel(logrus.InfoLevel)
	cmd.Stderr = h.log.WriterLevel(logrus.ErrorLevel)
	err := cmd.Run()

	return err
}
