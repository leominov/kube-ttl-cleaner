package main

import (
	"context"
	"fmt"
	"sort"
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"k8s.io/client-go/tools/clientcmd"
)

func NewKubernetesClient() (*kubernetes.Clientset, error) {
	loadingRules := clientcmd.NewDefaultClientConfigLoadingRules()
	kubeConfig := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(loadingRules, &clientcmd.ConfigOverrides{})
	config, err := kubeConfig.ClientConfig()
	if err != nil {
		return nil, err
	}
	return kubernetes.NewForConfig(config)
}

func RemovePVCCollection(clientset kubernetes.Interface, namespace, releaseName string) error {
	listOpts := metav1.ListOptions{
		LabelSelector: fmt.Sprintf("app.kubernetes.io/instance=%s", releaseName),
	}
	pvcList, _ := clientset.CoreV1().PersistentVolumeClaims(namespace).List(context.Background(), listOpts)
	var failedPVC []string
	for _, pvc := range pvcList.Items {
		err := clientset.CoreV1().PersistentVolumeClaims(namespace).Delete(context.Background(), pvc.Name, metav1.DeleteOptions{})
		if err != nil {
			failedPVC = append(failedPVC, pvc.Name)
		}
	}
	if len(failedPVC) == 0 {
		return nil
	}
	sort.Strings(failedPVC)
	return fmt.Errorf("failed to remove pvc: %s", strings.Join(failedPVC, ", "))
}
